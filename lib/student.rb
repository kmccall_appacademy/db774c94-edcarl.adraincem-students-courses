require 'byebug'

class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(course)
    conflicts = @courses.select { |el| course.conflicts_with?(el) }
    if conflicts.empty?
      @courses << course unless @courses.include?(course)
      @courses.last.students << self
    else
      raise_error
    end
  end

  def course_load
    course_hash = Hash.new(0)
    @courses.each { |el| course_hash[el.department] += el.credits }
    course_hash
  end

  # def has_conflict?(course)
  #   self.courses.each do |el|
  #     if el.time_block == course.time_block
  #       el.days.each { |day| return true if course.days.include?(day) }
  #     end
  #   end
  #   false
  # end

end
